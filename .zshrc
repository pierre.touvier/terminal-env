# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
    source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi
# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# For root user only, need to disable the some specific check before
# start anything:
export ZSH_DISABLE_COMPFIX="true"

# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="pitou-agnoster"
ZSH_THEME="powerlevel10k/powerlevel10k"

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
    git
    zsh-syntax-highlighting
    zsh-autosuggestions
    tmux-cssh
)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# source function and alias file
if [[ -f $HOME/terminal-env/.functions_and_alias ]]; then
    . $HOME/terminal-env/.functions_and_alias
fi
# source docker alias file
if [[ -f $HOME/terminal-env/.docker_alias ]]; then
    . $HOME/terminal-env/.docker_alias
fi
if command -v pyenv 1>/dev/null 2>&1; then
    eval "$(pyenv init -)"
fi

autoload bashcompinit && bashcompinit
complete -C '/usr/local/bin/aws_completer' aws

# add other file to source automatically
if [[ $(hostname) == *.vdi.s1.p.fti.net ]] ; then
    CACHED_DIR=''
else
    CACHED_DIR='.'
fi
DIR_TO_CREATE=$HOME/${CACHED_DIR}my_specific_function
mkdir -p $HOME/${CACHED_DIR}my_specific_function $HOME/${CACHED_DIR}my_own_scripts
for f in $(ls $DIR_TO_CREATE) ; do
    source $DIR_TO_CREATE/$f
done

PATH="/usr/local/bin:$HOME/${CACHED_DIR}my_own_scripts/:$HOME/terminal-env/more-scripts/:$PATH"

if [[ ! -f $HOME/terminal-env/.kubectl_zsh_completer ]] ; then
    kubectl completion zsh > $HOME/terminal-env/.kubectl_zsh_completer
    echo "return 0" >> $HOME/terminal-env/.kubectl_zsh_completer
fi
source $HOME/terminal-env/.kubectl_zsh_completer

# only for mac OS
if [[ $(uname) != "Linux" ]] ; then
    # key binding for mac only
    bindkey "^[[H" beginning-of-line
    bindkey "^[[F" end-of-line

    # source image tools utils
    if [[ -f $HOME/terminal-env/.imageUtils/imgls ]] ; then
        PATH="$HOME/terminal-env/.imageUtils/:$PATH"
    fi
fi

# Remove duplicate information in PATH after reload session
export PATH=$(echo $PATH | tr ':' '\n' | sort -u | tr '\n' ':' | sed 's/:$//')
export PATH=$PATH:$HOME/.nexustools

typeset -g POWERLEVEL9K_INSTANT_PROMPT=quiet
export PATH="/usr/local/opt/mysql-client/bin:$PATH"
