#!/bin/bash

workdir=$(pwd)


echo "Install zsh"
if [[ $(uname) != Linux ]] ; then
    brew install zsh
    brew tap homebrew/cask-fonts
    brew cask install font-hack-nerd-font
else
    sudo apt install -y zsh
fi
sudo sed -i "s@$(whoami):\/bin\/sh@$(whoami):\/bin\/zsh@g" /etc/passwd
#chsh -s $(which zsh)

echo "Install Oh My ZSH"
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
cp $workdir/agnoster.zsh-theme $HOME/.oh-my-zsh/custom/pitou-agnoster.zsh-theme

echo "Install other stuff"
echo "====> Install Powerline fonts"
cd $HOME
git clone https://github.com/powerline/fonts.git
cd fonts
bash ./install.sh
cd $HOME
echo "====> Install Syntax Highlighting Plugin"
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
echo "====> Install AutoSuggestion Plugin"
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
echo "====> Install p10k theme for omz..."
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

echo "Adding all *rc files stuff and aliases and create link for root env"
cd $HOME
pwd
for i in .profile .bashrc .vimrc .zshrc .p10k.zsh ; do
    if [[ -f $i ]] ; then
        sudo rm -f $i ~root/$i
    fi
    echo "ln -s $HOME/terminal-env/$i $i"
    ln -s $HOME/terminal-env/$i $i
    sudo ln -sf $HOME/terminal-env/$i ~root/$i
done
