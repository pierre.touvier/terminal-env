#!/bin/bash

prefix=$1

ls -l Capture* | grep -v total | awk -F'Capture' '{print "Capture"$2}' > file
# | sed 's/ /\\ /g' 
# echo $prefix
# cat file

while read line ; do
    jour=$(echo $line | awk '{print $3}' | awk -F'\' '{print $1}')
    heure=$(echo $line | awk '{print $5}' | awk -F'\' '{print $1}' | awk -F'.png' '{print $1}')
    mv "$line" "$prefix"_"$jour"_"$heure".png
    echo "-> renaming \"$line\" to ${prefix}_${jour}_${heure}.png"
done < file

rm file